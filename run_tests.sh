#!/bin/sh

NUMSHADERS="$1"
INTERVAL="$2"
CYCLES="$3"

echo "Starting shader compilation tests.."

for CYCLE in $(seq 1 "$CYCLES"); do
	echo
	echo 'Starting test cycle '"$CYCLE"
	sleep "$INTERVAL"'m'
	for TESTNO in $(seq 1 "$NUMSHADERS") ; do
		./test_compile_time glsl \
			-vert 'shaders/glsl'"$TESTNO"'.vert' \
			-frag 'shaders/glsl'"$TESTNO"'.frag'
		./test_compile_time spirv \
			-vert 'shaders/spirv'"$TESTNO"'.vert' \
			-frag 'shaders/spirv'"$TESTNO"'.frag'
	done
	sleep "$INTERVAL"'m'
	for TESTNO in $(seq 1 "$NUMSHADERS") ; do
		./test_compile_time spirv \
			-vert 'shaders/spirv'"$TESTNO"'.vert' \
			-frag 'shaders/spirv'"$TESTNO"'.frag'
		./test_compile_time glsl \
			-vert 'shaders/glsl'"$TESTNO"'.vert' \
			-frag 'shaders/glsl'"$TESTNO"'.frag'
	done
	echo 'End test cycle '"$CYCLE"
done
