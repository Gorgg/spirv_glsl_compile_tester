# spirv_glsl_compile_tester

Crude and imperfect program for testing relative compilation time of shaders written in GLSL and SPIR-V.

The shaders/ directory contains a test set of GLSL shaders and their SPIR-V counterparts, generated with glslang and sourced from Mesa's shader-db. The nonprerprocessed* shaders can be converted into the glsl* shaders with the cpp preprocessor, but will require some manual tweaking to be compilable into SPIR-V. Depending on whether or not you wish to include GLSL preprocessor performance in the benchmarks, you may choose to use the nonpreprocessed versions.

Compile the testing program with:
gcc -lGL -lGLEW -lglut -o test_compile_time test_compile_time.c

When running tests, make sure MESA_GLSL_CACHE_DISABLE is set in the environment.
