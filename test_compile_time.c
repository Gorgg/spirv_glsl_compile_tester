/**
 * Test compilation, link, draw time for very large shaders.
 * Command line arguments:
 *  spirv - input spirv shaders
 *  glsl - input glsl shaders
 * -vert [filename] - source for vertex shaders
 * -frag [filename] - source for fragment shaders
 *
 * Brian Paul
 * 3 Dec 2015
 */

#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <GL/glew.h>
#include <GL/freeglut.h>


#if defined(_MSC_VER)
#define snprintf _snprintf
#endif


static int langu = 0;
static char vert_filename[1024];
static char frag_filename[1024];
static int program;

GLuint
CompileShaderText(GLenum shaderType, const char *text, GLint length,
                  GLdouble *compile_time)
{
   GLuint shader;
   GLint stat;
   GLdouble t0, t1;

   shader = glCreateShader(shaderType);
   t0 = glutGet(GLUT_ELAPSED_TIME);
   if (langu == 1) {
      glShaderSource(shader, 1, (const GLchar **) &text, &length);
      glCompileShader(shader);
   } else if (langu == 2) {
      glShaderBinary(1, &shader, GL_SHADER_BINARY_FORMAT_SPIR_V_ARB,
         text, length);
      glSpecializeShaderARB(shader, "main", 0, NULL, NULL);
   } else {
      printf("error: internal language specification\n");
      exit(1);
   }
   glGetShaderiv(shader, GL_COMPILE_STATUS, &stat);
   t1 = glutGet(GLUT_ELAPSED_TIME);
   *compile_time = t1 - t0;
   if (!stat) {
      GLchar log[1000];
      GLsizei len;
      glGetShaderInfoLog(shader, 1000, &len, log);
      printf("Error: problem compiling shader: %s\n", log);
      exit(1);
   }
   else {
      /*printf("Shader compiled OK\n");*/
   }
   return shader;
}


/**
 * Read a shader from a file.
 */
GLuint
CompileShaderFile(GLenum shaderType, const char *filename,
                  GLdouble *compile_time)
{
   const int max = 100*1000;
   int n;
   char *buffer = (char*) malloc(max);
   GLuint shader;
   FILE *f;

   f = fopen(filename, "r");
   if (!f) {
      printf("Unable to open shader file %s\n", filename);
      free(buffer);
      return 0;
   }

   n = fread(buffer, 1, max, f);
   /*printf("read %d bytes from shader file %s\n", n, filename);*/
   if (n > 0) {
      buffer[n] = 0;
      shader = CompileShaderText(shaderType, buffer, n, compile_time);
   }
   else {
      fclose(f);
      free(buffer);
      printf("warning: empty file\n");
      return 0;
   }

   fclose(f);
   free(buffer);

   return shader;
}


GLuint
LinkShaders(GLuint vertShader, GLuint fragShader, GLdouble *link_time)
{
   GLuint program = glCreateProgram();
   GLdouble t0, t1;

   assert(vertShader && fragShader);

   if (!(strcmp(vert_filename, "") == 0))
      glAttachShader(program, vertShader);
   if (!(strcmp(frag_filename, "") == 0))
      glAttachShader(program, fragShader);

   t0 = glutGet(GLUT_ELAPSED_TIME);
   glLinkProgram(program);

   /* check link */
   {
      GLint stat;
      glGetProgramiv(program, GL_LINK_STATUS, &stat);
      if (!stat) {
         GLchar log[1000];
         GLsizei len;
         glGetProgramInfoLog(program, 1000, &len, log);
         printf("Shader link error:\n%s\n", log);
         return 0;
      }
   }
   
   t1 = glutGet(GLUT_ELAPSED_TIME);
   *link_time = t1 - t0;

   return program;

}

static GLuint
create_shader_program(GLdouble *vert_compile_time, GLdouble *frag_compile_time,
                      GLdouble *link_time, GLdouble *time_to_use)
{
   GLuint fragShader;
   GLuint vertShader;
   GLuint program;
   GLdouble t0, t1;

   t0 = glutGet(GLUT_ELAPSED_TIME);
   if (!(strcmp(vert_filename, "") == 0))
      vertShader = CompileShaderFile(GL_VERTEX_SHADER, vert_filename,
         vert_compile_time);
   if (!(strcmp(frag_filename, "") == 0))
      fragShader = CompileShaderFile(GL_FRAGMENT_SHADER, frag_filename,
         frag_compile_time);
   program = LinkShaders(vertShader, fragShader, link_time);
   glUseProgram(program);
   t1 = glutGet(GLUT_ELAPSED_TIME);

   *time_to_use = t1 - t0;

   assert(glGetError() == GL_NO_ERROR);

   return program;
}


static void
Init(void)
{
   GLdouble vert_compile_time, frag_compile_time, link_time,
      total_compile_time, total_link_time, time_to_use;

   if (!GLEW_VERSION_2_0) {
	  printf("error: not shader capable\n");
      exit(1);
   }

   total_compile_time = total_link_time = 0;

   program = create_shader_program(&vert_compile_time, &frag_compile_time, 
                                   &link_time, &time_to_use);
   total_compile_time += vert_compile_time;
   total_compile_time += frag_compile_time;
   total_link_time += link_time;

   if (!(strcmp(vert_filename, "") == 0))
      printf("Vertex shader compile time: %f ms\n", vert_compile_time);
   if (!(strcmp(frag_filename, "") == 0))
      printf("Fragment shader compile time: %f ms\n", frag_compile_time);
   printf("Total glCompileShader() time: %f ms\n", total_compile_time);
   printf("Total glLinkProgram() time: %f ms\n", total_link_time);
   printf("Time to program use: %f ms\n", time_to_use);
}


int
main(int argc, char *argv[])
{
   int i;
   GLenum err;

   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_RGBA);
   glutInitContextVersion(4, 2);
   glutInitContextProfile(GLUT_CORE_PROFILE);
   glutCreateWindow("Compile Test");
   if((err = glewInit()) != GLEW_OK) {
      printf("error: GLEW initialization failed:\n");
      printf("Error: %s\n", glewGetErrorString(err));
   }
   
   frag_filename[0] = vert_filename[0] = '\0';

   for (i = 1; i < argc; i++) {
      if (strcmp(argv[i], "spirv") == 0) {
		 if (!GLEW_ARB_gl_spirv) {
            printf("error: SPIR-V shaders not supported by OpenGL "
                   "implementation\n");
            exit(1);
         }
         else
         langu = 2;
      }
      else if (strcmp(argv[i], "glsl") == 0) {
         langu = 1;
      }
      else if (strcmp(argv[i], "-vert") == 0) {
         i++;
         strncpy(vert_filename, argv[i], 1024);
      }
      else if (strcmp(argv[i], "-frag") == 0) {
         i++;
         strncpy(frag_filename, argv[i], 1024);
      }
      else {
         printf("unexpected option: %s\n", argv[i]);
         exit(1);
      }
   }

   if (langu == 0) {
     printf("error: no shading language specified\n");
     exit(1);
   }
   
   printf("Parameters: Language:");
   if (langu == 1)
	  printf(" GLSL");
   else if (langu == 2)
      printf(" SPIR-V");
   printf(" Vertex:");
   if ((strcmp(vert_filename, "") == 0))
      printf(" None");
   else
      printf(" %s", vert_filename);
   printf(" Fragment:");
   if ((strcmp(frag_filename, "") == 0))
      printf(" None");
   else
      printf(" %s", frag_filename);
   printf("\n");

   Init();

   return 0;
}
