#version 420

layout(std140, binding = 0) uniform uniforms {
	mat3 NormalMatrix;
	mat4 ModelViewMatrix;
	mat4 ProjectionMatrix;
};

layout(location = 0) in vec4 att0;
layout(location = 1) out vec4 var0;
layout(location = 2) in vec2 att1;
layout(location = 3) out vec2 var1;
layout(location = 4) out vec3 varposition;
layout(location = 5) out vec3 varnormal;
layout(location = 6) in vec4 Vertex;
layout(location = 7) in vec3 Normal;

void main()
{
	vec4 co = ModelViewMatrix * Vertex;

	varposition = co.xyz;
	varnormal = NormalMatrix * Normal;
	gl_Position = ProjectionMatrix * co;

	var0 = att0;
	var1 = att1;
}
