#version 420

layout(location = 0) in vec4 Vertex;
layout(location = 1) in vec4 Color;
layout(location = 2) in vec4 MultiTexCoord0;
layout(location = 3) in vec4 MultiTexCoord1;
layout(location = 0) out vec2 source_texcoords;
layout(location = 1) out float mask_coverage;

layout(std140, binding = 0) uniform uniforms {
	mat4 ModelViewProjectionMatrix;
};

void main()
{
    gl_Position = ModelViewProjectionMatrix * Vertex;
    source_texcoords = MultiTexCoord0.xy;
    mask_coverage = MultiTexCoord0.a;
}
