#version 420

layout(binding = 0) uniform sampler2D source_sampler;
layout(location = 0) in vec2 source_texcoords;
layout(location = 1) in float mask_coverage;
layout(location = 0) out vec4 FragColor;

vec4 get_source()
{
    return texture(source_sampler, source_texcoords);
}
vec4 get_mask()
{
    return vec4(0, 0, 0, mask_coverage);
}
void main()
{
    FragColor = get_source() * get_mask().a;
}

