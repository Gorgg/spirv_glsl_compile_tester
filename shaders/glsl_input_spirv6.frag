#version 440
#extension GL_ARB_shading_language_420pack : enable
#extension GL_ARB_shader_storage_buffer_object : enable
#extension GL_ARB_shader_image_load_store : enable

struct VS_OUTPUT {
  vec4 pos;
  vec4 colors_0;
  vec4 colors_1;
  vec3 tex0;
  vec4 clipPos;
  float clipDist0;
  float clipDist1;
};

layout(binding = 0) uniform sampler2DArray samp[8];

layout(std140, binding = 1) uniform PSBlock {
 ivec4 color[4];
 ivec4 k[4];
 ivec4 alphaRef;
 vec4 texdim[8];
 ivec4 czbias[2];
 ivec4 cindscale[2];
 ivec4 cindmtx[6];
 ivec4 cfogcolor;
 ivec4 cfogi;
 vec4 cfogf[2];
 vec4 czslope;
 vec2 cefbscale;
 uint bpmem_genmode;
 uint bpmem_alphaTest;
 uint bpmem_fogParam3;
 uint bpmem_fogRangeBase;
 uint bpmem_dstalpha;
 uint bpmem_ztex_op;
 bool bpmem_late_ztest;
 bool bpmem_rgba6_format;
 bool bpmem_dither;
 bool bpmem_bounding_box;
 uvec4 bpmem_pack1[16];
 uvec4 bpmem_pack2[8];
 ivec4 konstLookup[32];
};

layout(location = 0) out vec4 ocol0;
layout(location = 1) out vec4 ocol1;

layout(location = 0) in VertexData {
  vec4 pos;
  vec4 colors_0;
  vec4 colors_1;
  vec3 tex0;
  vec4 clipPos;
  float clipDist0;
  float clipDist1;
} vs;



int idot(ivec3 x, ivec3 y)
{
 ivec3 tmp = x * y;
 return tmp.x + tmp.y + tmp.z;
}
int idot(ivec4 x, ivec4 y)
{
 ivec4 tmp = x * y;
 return tmp.x + tmp.y + tmp.z + tmp.w;
}

int iround(float x) { return int (round(x)); }
ivec2 iround(vec2 x) { return ivec2(round(x)); }
ivec3 iround(vec3 x) { return ivec3(round(x)); }
ivec4 iround(vec4 x) { return ivec4(round(x)); }

vec3 selectTexCoord(uint index) {
  switch (index) {
  case 0u:
    return vs.tex0;
  default:
    return vec3(0.0, 0.0, 0.0);
  }
}

ivec4 sampleTexture(uint sampler_num, vec2 uv) {
  return iround(texture(samp[sampler_num], vec3(uv, 0.0)) * 255.0);
}

ivec4 Swizzle(uint s, ivec4 color) {


  ivec4 ret;
  ret.r = color[bitfieldExtract((bpmem_pack2[(s * 2u)].y), 0, 2)];
  ret.g = color[bitfieldExtract((bpmem_pack2[(s * 2u)].y), 2, 2)];
  ret.b = color[bitfieldExtract((bpmem_pack2[(s * 2u + 1u)].y), 0, 2)];
  ret.a = color[bitfieldExtract((bpmem_pack2[(s * 2u + 1u)].y), 2, 2)];
  return ret;
}

int Wrap(int coord, uint mode) {
  if (mode == 0u)
    return coord;
  else if (mode < 6u)
    return coord & (0xfffe >> mode);
  else
    return 0;
}


int tevLerp(int A, int B, int C, int D, uint bias, bool op, bool alpha, uint shift) {

  C += C >> 7;


  if (bias == 1u) D += 128;
  else if (bias == 2u) D -= 128;

  int mix = (A << 8) + (B - A)*C;
  if (shift != 3u) {
    mix = mix << shift;
    D = D << shift;
  }

  if ((shift == 3u) == alpha)
    mix = mix + (op ? 127 : 128);

  int result = mix >> 8;


  if(op)
    result = D - result;
  else
    result = D + result;



  if (shift == 3u)
    result = result >> 1;
  return result;
}


ivec3 tevLerp3(ivec3 A, ivec3 B, ivec3 C, ivec3 D, uint bias, bool op, bool alpha, uint shift) {

  C += C >> 7;


  if (bias == 1u) D += 128;
  else if (bias == 2u) D -= 128;

  ivec3 mix = (A << 8) + (B - A)*C;
  if (shift != 3u) {
    mix = mix << shift;
    D = D << shift;
  }

  if ((shift == 3u) == alpha)
    mix = mix + (op ? 127 : 128);

  ivec3 result = mix >> 8;


  if(op)
    result = D - result;
  else
    result = D + result;



  if (shift == 3u)
    result = result >> 1;
  return result;
}



bool tevCompare(uint op, ivec3 color_A, ivec3 color_B) {
  switch (op) {
  case 0u:
    return (color_A.r > color_B.r);
  case 1u:
    return (color_A.r == color_B.r);
  case 2u:
    int A_16 = (color_A.r | (color_A.g << 8));
    int B_16 = (color_B.r | (color_B.g << 8));
    return A_16 > B_16;
  case 3u:
    return (color_A.r == color_B.r && color_A.g == color_B.g);
  case 4u:
    int A_24 = (color_A.r | (color_A.g << 8) | (color_A.b << 16));
    int B_24 = (color_B.r | (color_B.g << 8) | (color_B.b << 16));
    return A_24 > B_24;
  case 5u:
    return (color_A.r == color_B.r && color_A.g == color_B.g && color_A.b == color_B.b);
  default:
    return false;
  }
}


bool alphaCompare(int a, int b, uint compare) {
  switch (compare) {
  case 0u:
    return false;
  case 1u:
    return a < b;
  case 2u:
    return a == b;
  case 3u:
    return a <= b;
  case 4u:
    return a > b;
  case 5u:
    return a != b;
  case 6u:
    return a >= b;
  case 7u:
    return true;
  }
}

struct State {
  ivec4 Reg[4];
  ivec4 TexColor;
  int AlphaBump;
};
struct StageState {
  uint stage;
  uint order;
  uint cc;
  uint ac;
};

ivec4 getRasColor(State s, StageState ss, vec4 colors_0, vec4 colors_1);
ivec4 getKonstColor(State s, StageState ss);

ivec3 selectColorInput(State s, StageState ss, vec4 colors_0, vec4 colors_1, uint index) {
  switch (index) {
  case 0u:
    return s.Reg[0].rgb;
  case 1u:
    return s.Reg[0].aaa;
  case 2u:
    return s.Reg[1].rgb;
  case 3u:
    return s.Reg[1].aaa;
  case 4u:
    return s.Reg[2].rgb;
  case 5u:
    return s.Reg[2].aaa;
  case 6u:
    return s.Reg[3].rgb;
  case 7u:
    return s.Reg[3].aaa;
  case 8u:
    return s.TexColor.rgb;
  case 9u:
    return s.TexColor.aaa;
  case 10u:
    return getRasColor(s, ss, colors_0, colors_1).rgb;
  case 11u:
    return getRasColor(s, ss, colors_0, colors_1).aaa;
  case 12u:
    return ivec3(255, 255, 255);
  case 13u:
    return ivec3(128, 128, 128);
  case 14u:
    return getKonstColor(s, ss).rgb;
  case 15u:
    return ivec3(0, 0, 0);
  }
}

int selectAlphaInput(State s, StageState ss, vec4 colors_0, vec4 colors_1, uint index) {
  switch (index) {
  case 0u:
    return s.Reg[0].a;
  case 1u:
    return s.Reg[1].a;
  case 2u:
    return s.Reg[2].a;
  case 3u:
    return s.Reg[3].a;
  case 4u:
    return s.TexColor.a;
  case 5u:
    return getRasColor(s, ss, colors_0, colors_1).a;
  case 6u:
    return getKonstColor(s, ss).a;
  case 7u:
    return 0;
  }
}

ivec4 getTevReg(in State s, uint index) {
  switch (index) {
  case 0u:
    return s.Reg[0];
  case 1u:
    return s.Reg[1];
  case 2u:
    return s.Reg[2];
  case 3u:
    return s.Reg[3];
  default:
    return s.Reg[0];
  }
}

void setRegColor(inout State s, uint index, ivec3 color) {
  switch (index) {
  case 0u:
    s.Reg[0].rgb = color;
    break;
  case 1u:
    s.Reg[1].rgb = color;
    break;
  case 2u:
    s.Reg[2].rgb = color;
    break;
  case 3u:
    s.Reg[3].rgb = color;
    break;
  }
}

void setRegAlpha(inout State s, uint index, int alpha) {
  switch (index) {
  case 0u:
    s.Reg[0].a = alpha;
    break;
  case 1u:
    s.Reg[1].a = alpha;
    break;
  case 2u:
    s.Reg[2].a = alpha;
    break;
  case 3u:
    s.Reg[3].a = alpha;
    break;
  }
}



void main()
{
  vec4 rawpos = gl_FragCoord;
  ivec3 tevcoord = ivec3(0, 0, 0);
  State s;
  s.TexColor = ivec4(0, 0, 0, 0);
  s.AlphaBump = 0;

  s.Reg[0] = color[0];
  s.Reg[1] = color[1];
  s.Reg[2] = color[2];
  s.Reg[3] = color[3];
  uint num_stages = bitfieldExtract(bpmem_genmode, 10, 4);


  for(uint stage = 0u; stage <= num_stages; stage++)
  {
    StageState ss;
    ss.stage = stage;
    ss.cc = (bpmem_pack1[(stage)].xy).x;
    ss.ac = (bpmem_pack1[(stage)].xy).y;
    ss.order = (bpmem_pack2[(stage>>1)].x);
    if ((stage & 1u) == 1u)
      ss.order = ss.order >> 12;

    uint tex_coord = bitfieldExtract(ss.order, 3, 3);
    vec3 uv = selectTexCoord((tex_coord));
    ivec2 fixedPoint_uv = ivec2((uv.z == 0.0 ? uv.xy : (uv.xy / uv.z)) * texdim[tex_coord].zw);

    bool texture_enabled = (ss.order & 64u) != 0u;


    uint tevind = (bpmem_pack1[(stage)].z);
    if (tevind != 0u)
    {
      uint bs = bitfieldExtract(tevind, 7, 2);
      uint fmt = bitfieldExtract(tevind, 2, 2);
      uint bias = bitfieldExtract(tevind, 4, 3);
      uint bt = bitfieldExtract(tevind, 0, 2);
      uint mid = bitfieldExtract(tevind, 9, 4);

      ivec3 indcoord;
{
  uint iref = (bpmem_pack1[(bt)].w);
  if ( iref != 0u)
  {
    uint texcoord = bitfieldExtract(iref, 0, 3);
    uint texmap = bitfieldExtract(iref, 8, 3);
    vec3 uv = selectTexCoord((texcoord));
    ivec2 fixedPoint_uv = ivec2((uv.z == 0.0 ? uv.xy : (uv.xy / uv.z)) * texdim[texcoord].zw);

    if ((bt & 1u) == 0u)
      fixedPoint_uv = fixedPoint_uv >> cindscale[bt >> 1].xy;
    else
      fixedPoint_uv = fixedPoint_uv >> cindscale[bt >> 1].zw;

    indcoord = sampleTexture(texmap, vec2(fixedPoint_uv) * texdim[texmap].xy).abg;
  }
  else
  {
    indcoord = ivec3(0, 0, 0);
  }
}
      if (bs != 0u)
        s.AlphaBump = indcoord[bs - 1u];
      switch(fmt)
      {
      case 0u:
        indcoord.x = indcoord.x + ((bias & 1u) != 0u ? -128 : 0);
        indcoord.y = indcoord.y + ((bias & 2u) != 0u ? -128 : 0);
        indcoord.z = indcoord.z + ((bias & 4u) != 0u ? -128 : 0);
        s.AlphaBump = s.AlphaBump & 0xf8;
        break;
      case 1u:
        indcoord.x = (indcoord.x & 0x1f) + ((bias & 1u) != 0u ? 1 : 0);
        indcoord.y = (indcoord.y & 0x1f) + ((bias & 2u) != 0u ? 1 : 0);
        indcoord.z = (indcoord.z & 0x1f) + ((bias & 4u) != 0u ? 1 : 0);
        s.AlphaBump = s.AlphaBump & 0xe0;
        break;
      case 2u:
        indcoord.x = (indcoord.x & 0x0f) + ((bias & 1u) != 0u ? 1 : 0);
        indcoord.y = (indcoord.y & 0x0f) + ((bias & 2u) != 0u ? 1 : 0);
        indcoord.z = (indcoord.z & 0x0f) + ((bias & 4u) != 0u ? 1 : 0);
        s.AlphaBump = s.AlphaBump & 0xf0;
        break;
      case 3u:
        indcoord.x = (indcoord.x & 0x07) + ((bias & 1u) != 0u ? 1 : 0);
        indcoord.y = (indcoord.y & 0x07) + ((bias & 2u) != 0u ? 1 : 0);
        indcoord.z = (indcoord.z & 0x07) + ((bias & 4u) != 0u ? 1 : 0);
        s.AlphaBump = s.AlphaBump & 0xf8;
        break;
      }


      ivec2 indtevtrans = ivec2(0, 0);
      if ((mid & 3u) != 0u)
      {
        uint mtxidx = 2u * ((mid & 3u) - 1u);
        int shift = cindmtx[mtxidx].w;

        switch (mid >> 2)
        {
        case 0u:
          indtevtrans = ivec2(idot(cindmtx[mtxidx].xyz, indcoord), idot(cindmtx[mtxidx + 1u].xyz, indcoord)) >> 3;
          break;
        case 1u:
          indtevtrans = (fixedPoint_uv * indcoord.xx) >> 8;
          break;
        case 2u:
          indtevtrans = (fixedPoint_uv * indcoord.yy) >> 8;
          break;
        }

        if (shift >= 0)
          indtevtrans = indtevtrans >> shift;
        else
          indtevtrans = indtevtrans << ((-shift) & 31);
      }


      uint sw = bitfieldExtract(tevind, 13, 3);
      uint tw = bitfieldExtract(tevind, 16, 3);
      ivec2 wrapped_coord = ivec2(Wrap(fixedPoint_uv.x, sw), Wrap(fixedPoint_uv.y, tw));

      if ((tevind & 1048576u) != 0u)
        tevcoord.xy += wrapped_coord + indtevtrans;
      else
        tevcoord.xy = wrapped_coord + indtevtrans;


      tevcoord.xy = (tevcoord.xy << 8) >> 8;
    }
    else if (texture_enabled)
    {
      tevcoord.xy = fixedPoint_uv;
    }


    if(texture_enabled) {
      uint sampler_num = bitfieldExtract(ss.order, 0, 3);

      vec2 uv = (vec2(tevcoord.xy)) * texdim[sampler_num].xy;

      ivec4 color = sampleTexture(sampler_num, uv);

      uint swap = bitfieldExtract(ss.ac, 2, 2);
      s.TexColor = Swizzle(swap, color);
    } else {

      s.TexColor = ivec4(255, 255, 255, 255);
    }


    {

      uint color_a = bitfieldExtract(ss.cc, 12, 4);
      uint color_b = bitfieldExtract(ss.cc, 8, 4);
      uint color_c = bitfieldExtract(ss.cc, 4, 4);
      uint color_d = bitfieldExtract(ss.cc, 0, 4);
      uint color_bias = bitfieldExtract(ss.cc, 16, 2);
      bool color_op = bool(bitfieldExtract(ss.cc, 18, 1));
      bool color_clamp = bool(bitfieldExtract(ss.cc, 19, 1));
      uint color_shift = bitfieldExtract(ss.cc, 20, 2);
      uint color_dest = bitfieldExtract(ss.cc, 22, 2);
      uint color_compare_op = color_shift << 1 | uint(color_op);

      ivec3 color_A = selectColorInput(s, ss, vs.colors_0, vs.colors_1, color_a) & ivec3(255, 255, 255);
      ivec3 color_B = selectColorInput(s, ss, vs.colors_0, vs.colors_1, color_b) & ivec3(255, 255, 255);
      ivec3 color_C = selectColorInput(s, ss, vs.colors_0, vs.colors_1, color_c) & ivec3(255, 255, 255);
      ivec3 color_D = selectColorInput(s, ss, vs.colors_0, vs.colors_1, color_d);

      ivec3 color;
      if(color_bias != 3u) {
        color = tevLerp3(color_A, color_B, color_C, color_D, color_bias, color_op, false, color_shift);
      } else {

        if (color_compare_op == 6u) {

          color.r = (color_A.r > color_B.r) ? color_C.r : 0;
          color.g = (color_A.g > color_B.g) ? color_C.g : 0;
          color.b = (color_A.b > color_B.b) ? color_C.b : 0;
        } else if (color_compare_op == 7u) {

          color.r = (color_A.r == color_B.r) ? color_C.r : 0;
          color.g = (color_A.g == color_B.g) ? color_C.g : 0;
          color.b = (color_A.b == color_B.b) ? color_C.b : 0;
        } else {

          color = tevCompare(color_compare_op, color_A, color_B) ? color_C : ivec3(0, 0, 0);
        }
        color = color_D + color;
      }


      if (color_clamp)
        color = clamp(color, 0, 255);
      else
        color = clamp(color, -1024, 1023);


      setRegColor(s, color_dest, color);


      uint alpha_a = bitfieldExtract(ss.ac, 13, 3);
      uint alpha_b = bitfieldExtract(ss.ac, 10, 3);
      uint alpha_c = bitfieldExtract(ss.ac, 7, 3);
      uint alpha_d = bitfieldExtract(ss.ac, 4, 3);
      uint alpha_bias = bitfieldExtract(ss.ac, 16, 2);
      bool alpha_op = bool(bitfieldExtract(ss.ac, 18, 1));
      bool alpha_clamp = bool(bitfieldExtract(ss.ac, 19, 1));
      uint alpha_shift = bitfieldExtract(ss.ac, 20, 2);
      uint alpha_dest = bitfieldExtract(ss.ac, 22, 2);
      uint alpha_compare_op = alpha_shift << 1 | uint(alpha_op);

      int alpha_A;
      int alpha_B;
      if (alpha_bias != 3u || alpha_compare_op > 5u) {

        alpha_A = selectAlphaInput(s, ss, vs.colors_0, vs.colors_1, alpha_a) & 255;
        alpha_B = selectAlphaInput(s, ss, vs.colors_0, vs.colors_1, alpha_b) & 255;
      };
      int alpha_C = selectAlphaInput(s, ss, vs.colors_0, vs.colors_1, alpha_c) & 255;
      int alpha_D = selectAlphaInput(s, ss, vs.colors_0, vs.colors_1, alpha_d);


      int alpha;
      if(alpha_bias != 3u) {
        alpha = tevLerp(alpha_A, alpha_B, alpha_C, alpha_D, alpha_bias, alpha_op, true, alpha_shift);
      } else {
        if (alpha_compare_op == 6u) {

          alpha = (alpha_A > alpha_B) ? alpha_C : 0;
        } else if (alpha_compare_op == 7u) {

          alpha = (alpha_A == alpha_B) ? alpha_C : 0;
        } else {

          alpha = tevCompare(alpha_compare_op, color_A, color_B) ? alpha_C : 0;
        }
        alpha = alpha_D + alpha;
      }


      if (alpha_clamp)
        alpha = clamp(alpha, 0, 255);
      else
        alpha = clamp(alpha, -1024, 1023);


      setRegAlpha(s, alpha_dest, alpha);
    }
  }

  ivec4 TevResult;
  TevResult.xyz = getTevReg(s, bitfieldExtract((bpmem_pack1[(num_stages)].xy).x, 22, 2)).xyz;
  TevResult.w = getTevReg(s, bitfieldExtract((bpmem_pack1[(num_stages)].xy).y, 22, 2)).w;
  TevResult &= 255;

  int zCoord = int(rawpos.z * 16777216.0);
  zCoord = clamp(zCoord, 0, 0xFFFFFF);


  if ((bpmem_genmode & 524288u) != 0u) {
    vec2 screenpos = rawpos.xy * cefbscale.xy;

    screenpos.y = 528.0 - screenpos.y;
    zCoord = int(czslope.z + czslope.x * screenpos.x + czslope.y * screenpos.y);
 }


  int early_zCoord = zCoord;
  if (bpmem_ztex_op != 0u) {
    int ztex = int(czbias[1].w);


    ztex += idot(s.TexColor.xyzw, czbias[0].xyzw);
    ztex += (bpmem_ztex_op == 1u) ? zCoord : 0;
    zCoord = ztex & 0xFFFFFF;
  }



  int zbuffer_zCoord = bpmem_late_ztest ? zCoord : early_zCoord;
  gl_FragDepth = float(zbuffer_zCoord) / 16777216.0;

  if (bpmem_alphaTest != 0u) {
    bool comp0 = alphaCompare(TevResult.a, alphaRef.r, bitfieldExtract(bpmem_alphaTest, 16, 3));
    bool comp1 = alphaCompare(TevResult.a, alphaRef.g, bitfieldExtract(bpmem_alphaTest, 19, 3));


    switch (bitfieldExtract(bpmem_alphaTest, 22, 2)) {
    case 0u:
      if (comp0 && comp1) break; else discard; break;
    case 1u:
      if (comp0 || comp1) break; else discard; break;
    case 2u:
      if (comp0 != comp1) break; else discard; break;
    case 3u:
      if (comp0 == comp1) break; else discard; break;
    }
  }

  if (bpmem_dither) {


    ivec2 dither = ivec2(rawpos.xy) & 1;
    TevResult.rgb = (TevResult.rgb - (TevResult.rgb >> 6)) + abs(dither.y * 3 - dither.x * 2);
  }


  uint fog_function = bitfieldExtract(bpmem_fogParam3, 21, 3);
  if (fog_function != 0u) {

    float ze;
    if (bitfieldExtract(bpmem_fogParam3, 20, 1) == 0u) {


      ze = (cfogf[1].x * 16777216.0) / float(cfogi.y - (zCoord >> cfogi.w));
    } else {


      ze = cfogf[1].x * float(zCoord) / 16777216.0;
    }

    if (bool(bitfieldExtract(bpmem_fogRangeBase, 10, 1))) {




      float x_adjust = (2.0 * (rawpos.x / cfogf[0].y)) - 1.0 - cfogf[0].x;
      x_adjust = sqrt(x_adjust * x_adjust + cfogf[0].z * cfogf[0].z) / cfogf[0].z;
      ze *= x_adjust;
    }

    float fog = clamp(ze - cfogf[1].z, 0.0, 1.0);

    if (fog_function > 3u) {
      switch (fog_function) {
      case 4u:
        fog = 1.0 - exp2(-8.0 * fog);
        break;
      case 5u:
        fog = 1.0 - exp2(-8.0 * fog * fog);
        break;
      case 6u:
        fog = exp2(-8.0 * (1.0 - fog));
        break;
      case 7u:
        fog = 1.0 - fog;
        fog = exp2(-8.0 * fog * fog);
        break;
      }
    }

    int ifog = iround(fog * 256.0);
    TevResult.rgb = (TevResult.rgb * (256 - ifog) + cfogcolor.rgb * ifog) >> 8;
  }

  if (bpmem_rgba6_format)
    ocol0.rgb = vec3(TevResult.rgb >> 2) / 63.0;
  else
    ocol0.rgb = vec3(TevResult.rgb) / 255.0;

  if (bpmem_dstalpha != 0u)
    ocol0.a = float(bitfieldExtract(bpmem_dstalpha, 0, 8) >> 2) / 63.0;
  else
    ocol0.a = float(TevResult.a >> 2) / 63.0;




  ocol1 = vec4(0.0, 0.0, 0.0, float(TevResult.a) / 255.0);
}

ivec4 getRasColor(State s, StageState ss, vec4 colors_0, vec4 colors_1) {

  uint ras = bitfieldExtract(ss.order, 7, 3);
  if (ras < 2u) {
    ivec4 color = iround(((ras == 0u) ? colors_0 : colors_1) * 255.0);
    uint swap = bitfieldExtract(ss.ac, 0, 2);
    return Swizzle(swap, color);
  } else if (ras == 5u) {
    return ivec4(s.AlphaBump, s.AlphaBump, s.AlphaBump, s.AlphaBump);
  } else if (ras == 6u) {
    int normalized = s.AlphaBump | s.AlphaBump >> 5;
    return ivec4(normalized, normalized, normalized, normalized);
  } else {
    return ivec4(0, 0, 0, 0);
  }
}

ivec4 getKonstColor(State s, StageState ss) {


  uint tevksel = (bpmem_pack2[(ss.stage>>1)].y);
  if ((ss.stage & 1u) == 0u)
    return ivec4(konstLookup[bitfieldExtract(tevksel, 4, 5)].rgb, konstLookup[bitfieldExtract(tevksel, 9, 5)].a);
  else
    return ivec4(konstLookup[bitfieldExtract(tevksel, 14, 5)].rgb, konstLookup[bitfieldExtract(tevksel, 19, 5)].a);
}
