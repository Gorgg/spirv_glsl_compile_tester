#version 420

layout(binding = 0) uniform vertuniforms {
	mat4 proj;
};

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texcoord;
layout(location = 0) out vec2 v_texcoord;

void main(void)
{
   gl_Position = proj * vec4(position, 0.0, 1.0);
   v_texcoord = texcoord;
}
