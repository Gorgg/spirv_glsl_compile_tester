#version 420
#extension GL_ARB_texture_rectangle : enable

layout(binding = 0) uniform sampler2DRect tex0, tex1, tex3;
layout(binding = 1) uniform sampler2DRectShadow tex4;

layout(std140, binding = 1) uniform fraguniforms {
	vec4 lightpos[8];
	vec3 lightcolor[8];
	vec4 shadowparams[8];
	vec2 shadowoffset[8];
	vec3 splitcenter[3];
	vec3 splitbounds[3];
	vec3 splitscale[3];
	vec3 splitoffset[3];
	vec3 camera;
	mat4 worldmatrix;
	vec4 fogdir;
	vec3 fogcolor;
	vec2 fogdensity;
	vec4 radialfogscale;
	vec2 shadowatlasscale;
	vec4 lightscale;
	vec3 gdepthscale;
	vec3 gdepthunpackparams;
};

layout(location = 0) out vec4 FragColor;

        
                vec3 getshadowtc(vec3 dir, vec4 shadowparams, vec2 shadowoffset, float distbias)
                {
                    vec3 adir = abs(dir);
                    float m = max(adir.x, adir.y), mz = max(adir.z, m);
                    vec2 mparams = shadowparams.xy / max(mz + distbias, 1e-5);
                    vec4 proj;
                    if(adir.x > adir.y) proj = vec4(dir.zyx, 0.0); else proj = vec4(dir.xzy, 1.0);
                    if(adir.z > m) proj = vec4(dir, 2.0);
                    return vec3(proj.xy * mparams.x + vec2(proj.w, step(proj.z, 0.0)) * shadowparams.z + shadowoffset, mparams.y + shadowparams.w);
                }
            

        
                float filtershadow(vec3 shadowtc)
                {
                    vec2 offset = fract(shadowtc.xy - 0.5);
                    vec3 center = shadowtc;
                    
                    
                    
                    
                    
                    
                    
                    center.xy -= offset*0.5;
                    vec4 size = vec4(offset + 1.0, 2.0 - offset);
                    return (1.0/9.0)*dot(size.zxzx*size.wwyy,
                        vec4(float(texture(tex4, center + vec3(-0.5, -0.5, 0.0))),
                             float(texture(tex4, center + vec3(1.0, -0.5, 0.0))),
                             float(texture(tex4, center + vec3(-0.5, 1.0, 0.0))),
                             float(texture(tex4, center + vec3(1.0, 1.0, 0.0)))));
                }
            

        

        void main(void)
        {   
                vec4 normal = texture(tex1, gl_FragCoord.xy);

                    

                    normal.xyz = normal.xyz*2.0 - 1.0;
                    

                    vec4 diffuse = texture(tex0, gl_FragCoord.xy);
                    
                    diffuse.rgb *= normal.a;
                
            

            
                                
                
        float depth = texture(tex3, gl_FragCoord.xy).r;
        
                    vec4 pos = worldmatrix * vec4(gl_FragCoord.xy, depth, 1.0);
                    pos.xyz /= pos.w;
                
    
                
                    float fogcoord = length(camera - pos.xyz);                        
                    vec3 camdir = normalize(camera - pos.xyz);
                    float facing = 2.0*dot(normal.xyz, camdir);
                
                
            

            
                vec3 light = vec3(0.0);
            

            

            
                vec3 light0dir = pos.xyz * lightpos[0].w - lightpos[0].xyz;
                float light0dist2 = dot(light0dir, light0dir);
                if(light0dist2 < 1.0)
                {
                    
                    float light0facing = dot(light0dir, normal.xyz);
                    if(light0facing < 0.0)
                    {
                        float light0invdist = inversesqrt(light0dist2); 
                        
                        float light0atten = light0facing * (1.0 - light0invdist);
                        
                        
                        
                                vec3 shadow0tc = getshadowtc(light0dir, shadowparams[0], shadowoffset[0], -8.0 * lightpos[0].w);
                                light0atten *= filtershadow(shadow0tc);
                            
                        
                        
                            
                            float light0spec = pow(clamp(light0invdist*(dot(camdir, light0dir) - light0facing*facing), 0.0, 1.0), 16.0) * diffuse.a;
                            light += (diffuse.rgb + light0spec) * lightcolor[0] * light0atten;
                            
                        
                        
                    }
                }
             
                vec3 light1dir = pos.xyz * lightpos[1].w - lightpos[1].xyz;
                float light1dist2 = dot(light1dir, light1dir);
                if(light1dist2 < 1.0)
                {
                    
                    float light1facing = dot(light1dir, normal.xyz);
                    if(light1facing < 0.0)
                    {
                        float light1invdist = inversesqrt(light1dist2); 
                        
                        float light1atten = light1facing * (1.0 - light1invdist);
                        
                        
                        
                                vec3 shadow1tc = getshadowtc(light1dir, shadowparams[1], shadowoffset[1], -8.0 * lightpos[1].w);
                                light1atten *= filtershadow(shadow1tc);
                            
                        
                        
                            
                            float light1spec = pow(clamp(light1invdist*(dot(camdir, light1dir) - light1facing*facing), 0.0, 1.0), 16.0) * diffuse.a;
                            light += (diffuse.rgb + light1spec) * lightcolor[1] * light1atten;
                            
                        
                        
                    }
                }
             
                vec3 light2dir = pos.xyz * lightpos[2].w - lightpos[2].xyz;
                float light2dist2 = dot(light2dir, light2dir);
                if(light2dist2 < 1.0)
                {
                    
                    float light2facing = dot(light2dir, normal.xyz);
                    if(light2facing < 0.0)
                    {
                        float light2invdist = inversesqrt(light2dist2); 
                        
                        float light2atten = light2facing * (1.0 - light2invdist);
                        
                        
                        
                                vec3 shadow2tc = getshadowtc(light2dir, shadowparams[2], shadowoffset[2], -8.0 * lightpos[2].w);
                                light2atten *= filtershadow(shadow2tc);
                            
                        
                        
                            
                            float light2spec = pow(clamp(light2invdist*(dot(camdir, light2dir) - light2facing*facing), 0.0, 1.0), 16.0) * diffuse.a;
                            light += (diffuse.rgb + light2spec) * lightcolor[2] * light2atten;
                            
                        
                        
                    }
                }
             
                vec3 light3dir = pos.xyz * lightpos[3].w - lightpos[3].xyz;
                float light3dist2 = dot(light3dir, light3dir);
                if(light3dist2 < 1.0)
                {
                    
                    float light3facing = dot(light3dir, normal.xyz);
                    if(light3facing < 0.0)
                    {
                        float light3invdist = inversesqrt(light3dist2); 
                        
                        float light3atten = light3facing * (1.0 - light3invdist);
                        
                        
                        
                                vec3 shadow3tc = getshadowtc(light3dir, shadowparams[3], shadowoffset[3], -8.0 * lightpos[3].w);
                                light3atten *= filtershadow(shadow3tc);
                            
                        
                        
                            
                            float light3spec = pow(clamp(light3invdist*(dot(camdir, light3dir) - light3facing*facing), 0.0, 1.0), 16.0) * diffuse.a;
                            light += (diffuse.rgb + light3spec) * lightcolor[3] * light3atten;
                            
                        
                        
                    }
                }
             
                vec3 light4dir = pos.xyz * lightpos[4].w - lightpos[4].xyz;
                float light4dist2 = dot(light4dir, light4dir);
                if(light4dist2 < 1.0)
                {
                    
                    float light4facing = dot(light4dir, normal.xyz);
                    if(light4facing < 0.0)
                    {
                        float light4invdist = inversesqrt(light4dist2); 
                        
                        float light4atten = light4facing * (1.0 - light4invdist);
                        
                        
                        
                                vec3 shadow4tc = getshadowtc(light4dir, shadowparams[4], shadowoffset[4], -8.0 * lightpos[4].w);
                                light4atten *= filtershadow(shadow4tc);
                            
                        
                        
                            
                            float light4spec = pow(clamp(light4invdist*(dot(camdir, light4dir) - light4facing*facing), 0.0, 1.0), 16.0) * diffuse.a;
                            light += (diffuse.rgb + light4spec) * lightcolor[4] * light4atten;
                            
                        
                        
                    }
                }
             
                vec3 light5dir = pos.xyz * lightpos[5].w - lightpos[5].xyz;
                float light5dist2 = dot(light5dir, light5dir);
                if(light5dist2 < 1.0)
                {
                    
                    float light5facing = dot(light5dir, normal.xyz);
                    if(light5facing < 0.0)
                    {
                        float light5invdist = inversesqrt(light5dist2); 
                        
                        float light5atten = light5facing * (1.0 - light5invdist);
                        
                        
                        
                                vec3 shadow5tc = getshadowtc(light5dir, shadowparams[5], shadowoffset[5], -8.0 * lightpos[5].w);
                                light5atten *= filtershadow(shadow5tc);
                            
                        
                        
                            
                            float light5spec = pow(clamp(light5invdist*(dot(camdir, light5dir) - light5facing*facing), 0.0, 1.0), 16.0) * diffuse.a;
                            light += (diffuse.rgb + light5spec) * lightcolor[5] * light5atten;
                            
                        
                        
                    }
                }
             
                vec3 light6dir = pos.xyz * lightpos[6].w - lightpos[6].xyz;
                float light6dist2 = dot(light6dir, light6dir);
                if(light6dist2 < 1.0)
                {
                    
                    float light6facing = dot(light6dir, normal.xyz);
                    if(light6facing < 0.0)
                    {
                        float light6invdist = inversesqrt(light6dist2); 
                        
                        float light6atten = light6facing * (1.0 - light6invdist);
                        
                        
                        
                                vec3 shadow6tc = getshadowtc(light6dir, shadowparams[6], shadowoffset[6], -8.0 * lightpos[6].w);
                                light6atten *= filtershadow(shadow6tc);
                            
                        
                        
                            
                            float light6spec = pow(clamp(light6invdist*(dot(camdir, light6dir) - light6facing*facing), 0.0, 1.0), 16.0) * diffuse.a;
                            light += (diffuse.rgb + light6spec) * lightcolor[6] * light6atten;
                            
                        
                        
                    }
                }
             
                vec3 light7dir = pos.xyz * lightpos[7].w - lightpos[7].xyz;
                float light7dist2 = dot(light7dir, light7dir);
                if(light7dist2 < 1.0)
                {
                    
                    float light7facing = dot(light7dir, normal.xyz);
                    if(light7facing < 0.0)
                    {
                        float light7invdist = inversesqrt(light7dist2); 
                        
                        float light7atten = light7facing * (1.0 - light7invdist);
                        
                        
                        
                                vec3 shadow7tc = getshadowtc(light7dir, shadowparams[7], shadowoffset[7], -8.0 * lightpos[7].w);
                                light7atten *= filtershadow(shadow7tc);
                            
                        
                        
                            
                            float light7spec = pow(clamp(light7invdist*(dot(camdir, light7dir) - light7facing*facing), 0.0, 1.0), 16.0) * diffuse.a;
                            light += (diffuse.rgb + light7spec) * lightcolor[7] * light7atten;
                            
                        
                        
                    }
                }
            
            
                float foglerp = clamp(exp2(fogcoord*fogdensity.x)*fogdensity.y, 0.0, 1.0);
                
                    FragColor.rgb = (light*foglerp);
                    FragColor.a = 0.0;
                
            

            
        }
    

