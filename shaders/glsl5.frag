#version 420

precision mediump float;

layout(location = 0) in vec2 v_texcoord;
layout(location = 0) out vec4 FragColor;

layout(binding = 0) uniform sampler2D tex;

layout(binding = 1) uniform fraguniforms {
	float alpha;
	float texwidth;
};

void main()
{
   if (v_texcoord.x < 0.0 || v_texcoord.x > texwidth ||
       v_texcoord.y < 0.0 || v_texcoord.y > 1.0)
      discard;
   FragColor = texture(tex, v_texcoord);
   FragColor = alpha * FragColor;
}

