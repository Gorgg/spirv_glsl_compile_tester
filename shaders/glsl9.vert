#version 420

struct MaterialParameters {
    vec4  emission;    // Ecm
    vec4  ambient;     // Acm
    vec4  diffuse;     // Dcm
    vec4  specular;    // Scm
    float shininess;   // Srm
};

struct FogParameters {
    vec4 color;
    float density;
    float start;
    float end;
    float scale;
};

struct LightModelProducts {
    vec4  sceneColor;
};

struct LightSourceParameters 
{   
   vec4 ambient;              // Aclarri   
   vec4 diffuse;              // Dcli   
   vec4 specular;             // Scli   
   vec4 position;             // Ppli   
   vec4 halfVector;           // Derived: Hi   
   vec3 spotDirection;        // Sdli   
   float spotExponent;        // Srli   
   float spotCutoff;          // Crli                              
                              // (range: [0.0,90.0], 180.0)   
   float spotCosCutoff;       // Derived: cos(Crli)                 
                              // (range: [1.0,0.0],-1.0)   
   float constantAttenuation; // K0   
   float linearAttenuation;   // K1   
   float quadraticAttenuation;// K2  
};

layout(std140, binding = 0) uniform uniforms {
	float stretch;
	mat4 ModelViewProjectionMatrix;
	mat4 ModelViewMatrix;
	mat4 TextureMatrix;
	mat3 NormalMatrix;
	LightSourceParameters LightSource[4];
	vec4 teamcolour;
	int tcmask, normalmap;
	int fogEnabled;
	bool ecmEffect;
	float graphicsCycle;
	FogParameters Fog;
	LightModelProducts FrontLightModelProduct;
	MaterialParameters  FrontMaterial;
};

layout(location = 0) in vec4 Vertex;
layout(location = 1) in vec4 MultiTexCoord0;
layout(location = 2) in vec3 Normal;
layout(location = 3) in vec4 Color;
layout(location = 0) out float vertexDistance;
layout(location = 1) out vec3 normal;
layout(location = 2) out vec3 lightDir;
layout(location = 3) out vec3 eyeVec;
layout(location = 4) out vec4 FrontColor;
layout(location = 5) out vec4 TexCoord[1];

void main(void)
{
	vec3 vVertex = vec3(ModelViewMatrix * Vertex);
	vec4 position = Vertex;

	// Pass texture coordinates to fragment shader
	TexCoord[0] = TextureMatrix[0] * MultiTexCoord0;

	// Lighting -- we pass these to the fragment shader
	normal = NormalMatrix * Normal;
	lightDir = vec3(LightSource[0].position.xyz - vVertex);
	eyeVec = -vVertex;
	FrontColor = Color;

	// Implement building stretching to accomodate terrain
	if (position.y <= 0.0)
	{
		position.y -= stretch;
	}
	
	// Translate every vertex according to the Model View and Projection Matrix
	gl_Position = ModelViewProjectionMatrix * position;

	// Remember vertex distance
	vertexDistance = gl_Position.z;
}
