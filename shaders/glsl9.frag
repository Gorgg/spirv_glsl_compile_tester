#version 420

struct MaterialParameters {
    vec4  emission;    // Ecm
    vec4  ambient;     // Acm
    vec4  diffuse;     // Dcm
    vec4  specular;    // Scm
    float shininess;   // Srm
};

struct FogParameters {
    vec4 color;
    float density;
    float start;
    float end;
    float scale;
};

struct LightModelProducts {
    vec4  sceneColor;
};

struct LightSourceParameters 
{   
   vec4 ambient;              // Aclarri   
   vec4 diffuse;              // Dcli   
   vec4 specular;             // Scli   
   vec4 position;             // Ppli   
   vec4 halfVector;           // Derived: Hi   
   vec3 spotDirection;        // Sdli   
   float spotExponent;        // Srli   
   float spotCutoff;          // Crli                              
                              // (range: [0.0,90.0], 180.0)   
   float spotCosCutoff;       // Derived: cos(Crli)                 
                              // (range: [1.0,0.0],-1.0)   
   float constantAttenuation; // K0   
   float linearAttenuation;   // K1   
   float quadraticAttenuation;// K2  
};

layout(location = 0) in float vertexDistance;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 lightDir;
layout(location = 3) in vec3 eyeVec;
layout(location = 4) in vec4 FrontColor;
layout(location = 5) in vec4 TexCoord[1];
layout(location = 0) out vec4 FragColor;

layout(binding = 0) uniform sampler2D Texture0;
layout(binding = 1) uniform sampler2D Texture1;
layout(binding = 2) uniform sampler2D Texture2;

layout(std140, binding = 0) uniform uniforms {
	float stretch;
	mat4 ModelViewProjectionMatrix;
	mat4 ModelViewMatrix;
	mat4 TextureMatrix;
	mat3 NormalMatrix;
	LightSourceParameters LightSource[4];
	vec4 teamcolour;
	int tcmask, normalmap;
	int fogEnabled;
	bool ecmEffect;
	float graphicsCycle;
	FogParameters Fog;
	LightModelProducts FrontLightModelProduct;
	MaterialParameters  FrontMaterial;
};

void main(void)
{
	vec4 mask, colour;
	vec4 light = (FrontLightModelProduct.sceneColor * FrontMaterial.ambient) + (LightSource[0].ambient * FrontMaterial.ambient);
	vec3 N = normalize(normal);
	vec3 L = normalize(lightDir);
	float lambertTerm = dot(N, L);
	if (lambertTerm > 0.0)
	{
		light += LightSource[0].diffuse * FrontMaterial.diffuse * lambertTerm;
		vec3 E = normalize(eyeVec);
		vec3 R = reflect(-L, N);
		float specular = pow(max(dot(R, E), 0.0), FrontMaterial.shininess);
		light += LightSource[0].specular * FrontMaterial.specular * specular;
	}

	// Get color from texture unit 0, merge with lighting
	colour = texture(Texture0, TexCoord[0].st) * light;

	if (tcmask == 1)
	{
		// Get tcmask information from texture unit 1
		mask = texture(Texture1, TexCoord[0].st);
	
		// Apply color using grain merge with tcmask
		FragColor = (colour + (teamcolour - 0.5) * mask.a) * FrontColor;
	}
	else
	{
		FragColor = colour * FrontColor;
	}

	if (ecmEffect)
	{
		FragColor.a = 0.45 + 0.225 * graphicsCycle;
	}

	if (fogEnabled > 0)
	{
		// Calculate linear fog
		float fogFactor = (Fog.end - vertexDistance) / (Fog.end - Fog.start);
		fogFactor = clamp(fogFactor, 0.0, 1.0);
	
		// Return fragment color
		FragColor = mix(Fog.color, FragColor, fogFactor);
	}
}
