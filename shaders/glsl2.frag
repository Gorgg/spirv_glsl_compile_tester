#version 420

precision highp float;

layout(location = 14) in vec4 qf_FrontColor;
layout(location = 11) in vec3 v_Position;
layout(location = 12) in vec2 v_TexCoord;
layout(location = 13) in vec2 v_LightmapTexCoord[1];
layout(location = 0) out vec4 qf_FragColor;
layout(binding = 0) uniform sampler2D u_BaseTexture;
layout(binding = 1) uniform sampler2D u_LightmapTexture[1];

struct DynamicLight
{
 float Radius;
 vec3 Position;
 vec3 Diffuse;
};

layout(std140, binding = 0) uniform uniforms {
	vec3 u_QF_ViewOrigin;
	mat3 u_QF_ViewAxis;
	float u_QF_MirrorSide;
	vec3 u_QF_EntityOrigin;
	float u_QF_ShaderTime;
	mat4 u_ModelViewMatrix;
	mat4 u_ModelViewProjectionMatrix;
	float u_ShaderTime;
	vec3 u_ViewOrigin;
	mat3 u_ViewAxis;
	vec3 u_EntityDist;
	vec3 u_EntityOrigin;
	vec4 u_EntityColor;
	vec4 u_ConstColor;
	vec4 u_RGBGenFuncArgs;
	vec4 u_AlphaGenFuncArgs;
	vec3 u_LightstyleColor[4];
	vec3 u_LightAmbient;
	vec3 u_LightDiffuse;
	vec3 u_LightDir;
	vec2 u_BlendMix;
	vec2 u_TextureMatrix[3];
	float u_MirrorSide;
	float u_ZNear, u_ZFar;
	ivec4 u_Viewport;
	vec4 u_TextureParams;
	float u_SoftParticlesScale;
	vec4 u_QF_InstancePoints[2];
	vec4 u_QF_DualQuats[100*2];
	DynamicLight u_DynamicLights[4];
	int u_NumDynamicLights;
};

float QF_WaveFunc_Sin(float x)
{
x -= floor(x);
return sin(x * 6.28318530717958647692);
}
float QF_WaveFunc_Triangle(float x)
{
x -= floor(x);
return step(x, 0.25) * x * 4.0 + (2.0 - 4.0 * step(0.25, x) * step(x, 0.75) * x) + ((step(0.75, x) * x - 0.75) * 4.0 - 1.0);
}
float QF_WaveFunc_Square(float x)
{
x -= floor(x);
return step(x, 0.5) * 2.0 - 1.0;
}
float QF_WaveFunc_Sawtooth(float x)
{
x -= floor(x);
return x;
}
float QF_QF_WaveFunc_InverseSawtooth(float x)
{
x -= floor(x);
return 1.0 - x;
}

vec3 DynamicLightsSummaryColor(in vec3 Position)
{
 vec3 Color = vec3(0.0);
 for (int i = 0; i < u_NumDynamicLights; i++)
 {
  vec3 STR = vec3(u_DynamicLights[i].Position - Position);
  float distance = length(STR);
  float falloff = clamp(1.0 - distance / u_DynamicLights[i].Radius, 0.0, 1.0);
  falloff *= falloff;
  Color += falloff * u_DynamicLights[i].Diffuse;
 }
 return Color;
}

vec3 DynamicLightsSummaryColor(in vec3 Position, in vec3 surfaceNormalModelspace)
{
 vec3 Color = vec3(0.0);
 for (int i = 0; i < u_NumDynamicLights; i++)
 {
  vec3 STR = vec3(u_DynamicLights[i].Position - Position);
  float distance = length(STR);
  float falloff = clamp(1.0 - distance / u_DynamicLights[i].Radius, 0.0, 1.0);
  falloff *= falloff;
  falloff *= float(max(dot(normalize(STR), surfaceNormalModelspace), 0.0));
  Color += falloff * u_DynamicLights[i].Diffuse;
 }
 return Color;
}

void main(void)
{
 vec4 color;
 color = vec4(0.0, 0.0, 0.0, qf_FrontColor.a);
 color.rgb += vec3(texture(u_LightmapTexture[0], v_LightmapTexCoord[0])) * u_LightstyleColor[0];
 color.rgb += DynamicLightsSummaryColor(v_Position);
 vec4 diffuse;
 diffuse = vec4(texture(u_BaseTexture, v_TexCoord));
 color *= diffuse;
 color *= vec4(qf_FrontColor);
 qf_FragColor = vec4(color);
}
