#version 440

#extension GL_ARB_shading_language_420pack : enable
#extension GL_ARB_shader_storage_buffer_object : enable
#extension GL_ARB_shader_image_load_store : enable


struct Light {
 ivec4 color;
 vec4 cosatt;
 vec4 distatt;
 vec4 pos;
 vec4 dir;
};

layout(std140, binding = 2) uniform VSBlock {
 uint components;
 uint xfmem_dualTexInfo;
 uint xfmem_numColorChans;
 vec4 cpnmtx[6];
 vec4 cproj[4];
 ivec4 cmtrl[4];
 Light clights[8];
 vec4 ctexmtx[24];
 vec4 ctrmtx[64];
 vec4 cnmtx[32];
 vec4 cpostmtx[64];
 vec4 cpixelcenter;
 vec2 cviewport;
 uvec4 xfmem_pack1[8];
};

struct VS_OUTPUT {
  vec4 pos;
  vec4 colors_0;
  vec4 colors_1;
  vec3 tex0;
  vec4 clipPos;
  float clipDist0;
  float clipDist1;
};

layout(location = 0) in vec4 rawpos;
layout(location = 1) in uvec4 posmtx;
layout(location = 2) in vec3 rawnorm0;
layout(location = 3) in vec3 rawnorm1;
layout(location = 4) in vec3 rawnorm2;
layout(location = 5) in vec4 rawcolor0;
layout(location = 6) in vec4 rawcolor1;
layout(location = 7) in vec3 rawtex0;
layout(location = 8) in vec3 rawtex1;
layout(location = 9) in vec3 rawtex2;
layout(location = 10) in vec3 rawtex3;
layout(location = 11) in vec3 rawtex4;
layout(location = 12) in vec3 rawtex5;
layout(location = 13) in vec3 rawtex6;
layout(location = 14) in vec3 rawtex7;

layout(location = 0) out VertexData {
  vec4 pos;
  vec4 colors_0;
  vec4 colors_1;
  vec3 tex0;
  vec4 clipPos;
  float clipDist0;
  float clipDist1;
} vs;

ivec4 CalculateLighting(uint index, uint attnfunc, uint diffusefunc, vec3 pos, vec3 normal) {
  vec3 ldir, h, cosAttn, distAttn;
  float dist, dist2, attn;

  switch (attnfunc) {
  case 0u:
  case 2u:
    ldir = normalize(clights[index].pos.xyz - pos.xyz);
    attn = 1.0;
    if (length(ldir) == 0.0)
      ldir = normal;
    break;

  case 1u:
    ldir = normalize(clights[index].pos.xyz - pos.xyz);
    attn = (dot(normal, ldir) >= 0.0) ? max(0.0, dot(normal, clights[index].dir.xyz)) : 0.0;
    cosAttn = clights[index].cosatt.xyz;
    if (diffusefunc == 0u)
      distAttn = clights[index].distatt.xyz;
    else
      distAttn = normalize(clights[index].distatt.xyz);
    attn = max(0.0, dot(cosAttn, vec3(1.0, attn, attn*attn))) / dot(distAttn, vec3(1.0, attn, attn*attn));
    break;

  case 3u:
    ldir = clights[index].pos.xyz - pos.xyz;
    dist2 = dot(ldir, ldir);
    dist = sqrt(dist2);
    ldir = ldir / dist;
    attn = max(0.0, dot(ldir, clights[index].dir.xyz));
    attn = max(0.0, clights[index].cosatt.x + clights[index].cosatt.y * attn + clights[index].cosatt.z * attn * attn) / dot(clights[index].distatt.xyz, vec3(1.0, dist, dist2));
    break;

  default:
    attn = 1.0;
    ldir = normal;
    break;
  }

  switch (diffusefunc) {
  case 0u:
    return ivec4(round(attn * vec4(clights[index].color)));

  case 1u:
    return ivec4(round(attn * dot(ldir, normal) * vec4(clights[index].color)));

  case 2u:
    return ivec4(round(attn * max(0.0, dot(ldir, normal)) * vec4(clights[index].color)));

  default:
    return ivec4(0, 0, 0, 0);
  }
}

void main()
{
VS_OUTPUT o;


vec4 P0;
vec4 P1;
vec4 P2;


vec3 N0;
vec3 N1;
vec3 N2;

if ((components & 2u) != 0u) {

  int posidx = int(posmtx.r);
  P0 = ctrmtx[posidx];
  P1 = ctrmtx[posidx+1];
  P2 = ctrmtx[posidx+2];

  int normidx = posidx >= 32 ? (posidx - 32) : posidx;
  N0 = cnmtx[normidx].xyz;
  N1 = cnmtx[normidx+1].xyz;
  N2 = cnmtx[normidx+2].xyz;
} else {

  P0 = cpnmtx[0];
  P1 = cpnmtx[1];
  P2 = cpnmtx[2];
  N0 = cpnmtx[3].xyz;
  N1 = cpnmtx[4].xyz;
  N2 = cpnmtx[5].xyz;
}

vec4 pos = vec4(dot(P0, rawpos), dot(P1, rawpos), dot(P2, rawpos), 1.0);
o.pos = vec4(dot(cproj[0], pos), dot(cproj[1], pos), dot(cproj[2], pos), dot(cproj[3], pos));


vec3 _norm0 = vec3(0.0, 0.0, 0.0);
if ((components & 1024u) != 0u)
  _norm0 = normalize(vec3(dot(N0, rawnorm0), dot(N1, rawnorm0), dot(N2, rawnorm0)));

vec3 _norm1 = vec3(0.0, 0.0, 0.0);
if ((components & 2048u) != 0u)
  _norm1 = vec3(dot(N0, rawnorm1), dot(N1, rawnorm1), dot(N2, rawnorm1));

vec3 _norm2 = vec3(0.0, 0.0, 0.0);
if ((components & 4096u) != 0u)
  _norm2 = vec3(dot(N0, rawnorm2), dot(N1, rawnorm2), dot(N2, rawnorm2));


for (uint chan = 0u; chan < xfmem_numColorChans; chan++) {
  uint colorreg = (xfmem_pack1[(chan)].z);
  uint alphareg = (xfmem_pack1[(chan)].w);
  ivec4 mat = cmtrl[chan + 2u];
  ivec4 lacc = ivec4(255, 255, 255, 255);

  if (bitfieldExtract(colorreg, 0, 1) != 0u) {
    if ((components & (8192u << chan)) != 0u)
      mat.xyz = ivec3(round(((chan == 0u) ? rawcolor0.xyz : rawcolor1.xyz) * 255.0));
    else if ((components & 8192u) != 0u)
      mat.xyz = ivec3(round(rawcolor0.xyz * 255.0));
    else
      mat.xyz = ivec3(255, 255, 255);
  }

  if (bitfieldExtract(alphareg, 0, 1) != 0u) {
    if ((components & (8192u << chan)) != 0u)
      mat.w = int(round(((chan == 0u) ? rawcolor0.w : rawcolor1.w) * 255.0));
    else if ((components & 8192u) != 0u)
      mat.w = int(round(rawcolor0.w * 255.0));
    else
      mat.w = 255;
  } else {
    mat.w = cmtrl [chan + 2u].w;
  }

  if (bitfieldExtract(colorreg, 1, 1) != 0u) {
    if (bitfieldExtract(colorreg, 6, 1) != 0u) {
      if ((components & (8192u << chan)) != 0u)
        lacc.xyz = ivec3(round(((chan == 0u) ? rawcolor0.xyz : rawcolor1.xyz) * 255.0));
      else if ((components & 8192u) != 0u)
        lacc.xyz = ivec3(round(rawcolor0.xyz * 255.0));
      else
        lacc.xyz = ivec3(255, 255, 255);
    } else {
      lacc.xyz = cmtrl [chan].xyz;
    }

    uint light_mask = bitfieldExtract(colorreg, 2, 4) | (bitfieldExtract(colorreg, 11, 4) << 4u);
    uint attnfunc = bitfieldExtract(colorreg, 9, 2);
    uint diffusefunc = bitfieldExtract(colorreg, 7, 2);
    for (uint light_index = 0u; light_index < 8u; light_index++) {
      if ((light_mask & (1u << light_index)) != 0u)
        lacc.xyz += CalculateLighting(light_index, attnfunc, diffusefunc, pos.xyz, _norm0).xyz;
    }
  }

  if (bitfieldExtract(alphareg, 1, 1) != 0u) {
    if (bitfieldExtract(alphareg, 6, 1) != 0u) {
      if ((components & (8192u << chan)) != 0u)
        lacc.w = int(round(((chan == 0u) ? rawcolor0.w : rawcolor1.w) * 255.0));
      else if ((components & 8192u) != 0u)
        lacc.w = int(round(rawcolor0.w * 255.0));
      else
        lacc.w = 255;
    } else {
      lacc.w = cmtrl [chan].w;
    }

    uint light_mask = bitfieldExtract(alphareg, 2, 4) | (bitfieldExtract(alphareg, 11, 4) << 4u);
    uint attnfunc = bitfieldExtract(alphareg, 9, 2);
    uint diffusefunc = bitfieldExtract(alphareg, 7, 2);
    for (uint light_index = 0u; light_index < 8u; light_index++) {

      if ((light_mask & (1u << light_index)) != 0u)

        lacc.w += CalculateLighting(light_index, attnfunc, diffusefunc, pos.xyz, _norm0).w;
    }
  }

  lacc = clamp(lacc, 0, 255);


  vec4 lit_color = vec4((mat * (lacc + (lacc >> 7))) >> 8) / 255.0;
  switch (chan) {
  case 0u: o.colors_0 = lit_color; break;
  case 1u: o.colors_1 = lit_color; break;
  }
}

if (xfmem_numColorChans < 2u && (components & 16384u) == 0u)
  o.colors_1 = o.colors_0;

o.tex0 = vec3(0.0, 0.0, 0.0);

{ const uint texgen = 0u;

  vec4 coord = vec4(0.0, 0.0, 1.0, 1.0);
  uint texMtxInfo = (xfmem_pack1[(texgen)].x);
  switch (bitfieldExtract(texMtxInfo, 7, 5)) {
  case 0u:
    coord.xyz = rawpos.xyz;
    break;

  case 1u:
    coord.xyz = ((components & 1024u ) != 0u) ? rawnorm0.xyz : coord.xyz; break;

  case 3u:
    coord.xyz = ((components & 2048u ) != 0u) ? rawnorm1.xyz : coord.xyz; break;

  case 4u:
    coord.xyz = ((components & 4096u ) != 0u) ? rawnorm2.xyz : coord.xyz; break;

  case 5u:
    coord = ((components & 32768u ) != 0u) ? vec4(rawtex0.x, rawtex0.y, 1.0, 1.0) : coord;
    break;

  case 6u:
    coord = ((components & 65536u ) != 0u) ? vec4(rawtex1.x, rawtex1.y, 1.0, 1.0) : coord;
    break;

  case 7u:
    coord = ((components & 131072u ) != 0u) ? vec4(rawtex2.x, rawtex2.y, 1.0, 1.0) : coord;
    break;

  case 8u:
    coord = ((components & 262144u ) != 0u) ? vec4(rawtex3.x, rawtex3.y, 1.0, 1.0) : coord;
    break;

  case 9u:
    coord = ((components & 524288u ) != 0u) ? vec4(rawtex4.x, rawtex4.y, 1.0, 1.0) : coord;
    break;

  case 10u:
    coord = ((components & 1048576u ) != 0u) ? vec4(rawtex5.x, rawtex5.y, 1.0, 1.0) : coord;
    break;

  case 11u:
    coord = ((components & 2097152u ) != 0u) ? vec4(rawtex6.x, rawtex6.y, 1.0, 1.0) : coord;
    break;

  case 12u:
    coord = ((components & 4194304u ) != 0u) ? vec4(rawtex7.x, rawtex7.y, 1.0, 1.0) : coord;
    break;

  }


  if (bitfieldExtract(texMtxInfo, 2, 1) == 0u)
    coord.z = 1.0f;


  uint texgentype = bitfieldExtract(texMtxInfo, 4, 3);
  vec3 output_tex;
  switch (texgentype)
  {
  case 1u:
    {
      uint light = bitfieldExtract(texMtxInfo, 15, 3);
      uint source = bitfieldExtract(texMtxInfo, 12, 3);
      switch (source) {
      case 0u: output_tex.xyz = o.tex0; break;
      default: output_tex.xyz = vec3(0.0, 0.0, 0.0); break;
      }
      if ((components & 6144u) != 0u) {
        vec3 ldir = normalize(clights[light].pos.xyz - pos.xyz);
        output_tex.xyz += vec3(dot(ldir, _norm1), dot(ldir, _norm2), 0.0);
      }
    }
    break;

  case 2u:
    output_tex.xyz = vec3(o.colors_0.x, o.colors_0.y, 1.0);
    break;

  case 3u:
    output_tex.xyz = vec3(o.colors_1.x, o.colors_1.y, 1.0);
    break;

  default:
    {
      if ((components & (4u << texgen)) != 0u) {


        int tmp = 0;
        switch (texgen) {
        case 0u: tmp = int(rawtex0.z); break;
        }

        if (bitfieldExtract(texMtxInfo, 1, 1) == 1u) {
          output_tex.xyz = vec3(dot(coord, ctrmtx[tmp]),
                                  dot(coord, ctrmtx[tmp + 1]),
                                  dot(coord, ctrmtx[tmp + 2]));
        } else {
          output_tex.xyz = vec3(dot(coord, ctrmtx[tmp]),
                                  dot(coord, ctrmtx[tmp + 1]),
                                  1.0);
        }
      } else {
        if (bitfieldExtract(texMtxInfo, 1, 1) == 1u) {
          output_tex.xyz = vec3(dot(coord, ctexmtx[3u * texgen]),
                                  dot(coord, ctexmtx[3u * texgen + 1u]),
                                  dot(coord, ctexmtx[3u * texgen + 2u]));
        } else {
          output_tex.xyz = vec3(dot(coord, ctexmtx[3u * texgen]),
                                  dot(coord, ctexmtx[3u * texgen + 1u]),
                                  1.0);
        }
      }
    }
    break;

  }

  if (xfmem_dualTexInfo != 0u) {
    uint postMtxInfo = (xfmem_pack1[(texgen)].y); uint base_index = bitfieldExtract(postMtxInfo, 0, 6);
    vec4 P0 = cpostmtx[base_index & 0x3fu];
    vec4 P1 = cpostmtx[(base_index + 1u) & 0x3fu];
    vec4 P2 = cpostmtx[(base_index + 2u) & 0x3fu];

    if (bitfieldExtract(postMtxInfo, 8, 1) != 0u)
      output_tex.xyz = normalize(output_tex.xyz);


    output_tex.xyz = vec3(dot(P0.xyz, output_tex.xyz) + P0.w,
                            dot(P1.xyz, output_tex.xyz) + P1.w,
                            dot(P2.xyz, output_tex.xyz) + P2.w);
  }

  if (texgentype == 0u && output_tex.z == 0.0)
    output_tex.xy = clamp(output_tex.xy / 2.0f, vec2(-1.0f,-1.0f), vec2(1.0f,1.0f));


  switch (texgen) {
  case 0u: o.tex0 = output_tex; break;
  }
}
o.clipPos = o.pos;
float clipDepth = o.pos.z * (1.0 - 1e-7);
o.clipDist0 = clipDepth + o.pos.w;
o.clipDist1 = -clipDepth;
o.pos.z = o.pos.w * cpixelcenter.w - o.pos.z * cpixelcenter.z;
o.pos.xy *= sign(cpixelcenter.xy * vec2(1.0, -1.0));
o.pos.xy = o.pos.xy - o.pos.w * cpixelcenter.xy;
 vs.pos = o.pos;
 vs.colors_0 = o.colors_0;
 vs.colors_1 = o.colors_1;
 vs.tex0 = o.tex0;
 vs.clipPos = o.clipPos;
 vs.clipDist0 = o.clipDist0;
 vs.clipDist1 = o.clipDist1;
gl_ClipDistance[0] = o.clipDist0;
gl_ClipDistance[1] = o.clipDist1;
gl_Position = o.pos;
}
