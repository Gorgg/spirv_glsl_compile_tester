#version 420
#extension GL_ARB_draw_instanced : enable

precision highp float;

struct DynamicLight
{
 float Radius;
 vec3 Position;
 vec3 Diffuse;
};

layout(std140, binding = 0) uniform uniforms {
	vec3 u_QF_ViewOrigin;
	mat3 u_QF_ViewAxis;
	float u_QF_MirrorSide;
	vec3 u_QF_EntityOrigin;
	float u_QF_ShaderTime;
	mat4 u_ModelViewMatrix;
	mat4 u_ModelViewProjectionMatrix;
	float u_ShaderTime;
	vec3 u_ViewOrigin;
	mat3 u_ViewAxis;
	vec3 u_EntityDist;
	vec3 u_EntityOrigin;
	vec4 u_EntityColor;
	vec4 u_ConstColor;
	vec4 u_RGBGenFuncArgs;
	vec4 u_AlphaGenFuncArgs;
	vec3 u_LightstyleColor[4];
	vec3 u_LightAmbient;
	vec3 u_LightDiffuse;
	vec3 u_LightDir;
	vec2 u_BlendMix;
	vec2 u_TextureMatrix[3];
	float u_MirrorSide;
	float u_ZNear, u_ZFar;
	ivec4 u_Viewport;
	vec4 u_TextureParams;
	float u_SoftParticlesScale;
	vec4 u_QF_InstancePoints[2];
	vec4 u_QF_DualQuats[100*2];
	DynamicLight u_DynamicLights[4];
	int u_NumDynamicLights;
};

layout(location = 0) in vec4 a_Position;
layout(location = 1) in vec4 a_SVector;
layout(location = 2) in vec4 a_Normal;
layout(location = 3) in vec4 a_Color;
layout(location = 4) in vec2 a_TexCoord;
layout(location = 5) in vec2 a_LightmapCoord0;
layout(location = 6) in vec2 a_LightmapCoord1;
layout(location = 7) in vec2 a_LightmapCoord2;
layout(location = 8) in vec2 a_LightmapCoord3;
layout(location = 9) in vec4 a_BonesIndices;
layout(location = 10) in vec4 a_BonesWeights;
layout(location = 11) out vec3 v_Position;
layout(location = 12) out vec2 v_TexCoord;
layout(location = 13) out vec2 v_LightmapTexCoord[1];
layout(location = 14) out vec4 qf_FrontColor;

float QF_WaveFunc_Sin(float x)
{
x -= floor(x);
return sin(x * 6.28318530717958647692);
}
float QF_WaveFunc_Triangle(float x)
{
x -= floor(x);
return step(x, 0.25) * x * 4.0 + (2.0 - 4.0 * step(0.25, x) * step(x, 0.75) * x) + ((step(0.75, x) * x - 0.75) * 4.0 - 1.0);
}
float QF_WaveFunc_Square(float x)
{
x -= floor(x);
return step(x, 0.5) * 2.0 - 1.0;
}
float QF_WaveFunc_Sawtooth(float x)
{
x -= floor(x);
return x;
}
float QF_QF_WaveFunc_InverseSawtooth(float x)
{
x -= floor(x);
return 1.0 - x;
}

void QF_VertexDualQuatsTransform(const int numWeights, inout vec4 Position)
{
int index;
vec4 Indices = a_BonesIndices;
vec4 Weights = a_BonesWeights;
vec4 Indices_2 = Indices * 2.0;
vec4 DQReal, DQDual;

index = int(Indices_2.x);
DQReal = u_QF_DualQuats[index+0];
DQDual = u_QF_DualQuats[index+1];

if (numWeights > 1)
{
DQReal *= Weights.x;
DQDual *= Weights.x;

vec4 DQReal1, DQDual1;
float scale;

index = int(Indices_2.y);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];
scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.y;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;

if (numWeights > 2)
{
index = int(Indices_2.z);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];
scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.z;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;

if (numWeights > 3)
{
index = int(Indices_2.w);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];
scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.w;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;
}
}
}

float len = length(DQReal);
DQReal /= len;
DQDual /= len;

Position.xyz = (cross(DQReal.xyz, cross(DQReal.xyz, Position.xyz) + Position.xyz*DQReal.w + DQDual.xyz) + DQDual.xyz*DQReal.w - DQReal.xyz*DQDual.w)*2.0 + Position.xyz;
}
void QF_VertexDualQuatsTransform(const int numWeights, inout vec4 Position, inout vec3 Normal)
{
int index;
vec4 Indices = a_BonesIndices;
vec4 Weights = a_BonesWeights;
vec4 Indices_2 = Indices * 2.0;
vec4 DQReal, DQDual;

index = int(Indices_2.x);
DQReal = u_QF_DualQuats[index+0];
DQDual = u_QF_DualQuats[index+1];

if (numWeights > 1)
{
DQReal *= Weights.x;
DQDual *= Weights.x;

vec4 DQReal1, DQDual1;
float scale;

index = int(Indices_2.y);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];

scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.y;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;

if (numWeights > 2)
{
index = int(Indices_2.z);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];

scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.z;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;

if (numWeights > 3)
{
index = int(Indices_2.w);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];

scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.w;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;
}
}
}

float len = length(DQReal);
DQReal /= len;
DQDual /= len;
Position.xyz = (cross(DQReal.xyz, cross(DQReal.xyz, Position.xyz) + Position.xyz*DQReal.w + DQDual.xyz) + DQDual.xyz*DQReal.w - DQReal.xyz*DQDual.w)*2.0 + Position.xyz;
Normal = cross(DQReal.xyz, cross(DQReal.xyz, Normal) + Normal*DQReal.w)*2.0 + Normal;
}

void QF_VertexDualQuatsTransform(const int numWeights, inout vec4 Position, inout vec3 Normal, inout vec3 Tangent)
{
int index;
vec4 Indices = a_BonesIndices;
vec4 Weights = a_BonesWeights;
vec4 Indices_2 = Indices * 2.0;
vec4 DQReal, DQDual;

index = int(Indices_2.x);
DQReal = u_QF_DualQuats[index+0];
DQDual = u_QF_DualQuats[index+1];

if (numWeights > 1)
{
DQReal *= Weights.x;
DQDual *= Weights.x;

vec4 DQReal1, DQDual1;
float scale;

index = int(Indices_2.y);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];

scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.y;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;

if (numWeights > 2)
{
index = int(Indices_2.z);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];

scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.z;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;

if (numWeights > 3)
{
index = int(Indices_2.w);
DQReal1 = u_QF_DualQuats[index+0];
DQDual1 = u_QF_DualQuats[index+1];

scale = (dot(DQReal1, DQReal) < 0.0 ? -1.0 : 1.0) * Weights.w;
DQReal += DQReal1 * scale;
DQDual += DQDual1 * scale;
}
}
}

float len = length(DQReal);
DQReal /= len;
DQDual /= len;
Position.xyz = (cross(DQReal.xyz, cross(DQReal.xyz, Position.xyz) + Position.xyz*DQReal.w + DQDual.xyz) + DQDual.xyz*DQReal.w - DQReal.xyz*DQDual.w)*2.0 + Position.xyz;
Normal = cross(DQReal.xyz, cross(DQReal.xyz, Normal) + Normal*DQReal.w)*2.0 + Normal;
Tangent = cross(DQReal.xyz, cross(DQReal.xyz, Tangent) + Tangent*DQReal.w)*2.0 + Tangent;
}

void QF_InstancedTransform(inout vec4 Position, inout vec3 Normal)
{
Position.xyz = (cross(u_QF_InstancePoints[0].xyz, cross(u_QF_InstancePoints[0].xyz, Position.xyz) + Position.xyz*u_QF_InstancePoints[0].w)*2.0 + Position.xyz) *
 u_QF_InstancePoints[1].w + u_QF_InstancePoints[1].xyz;
Normal = cross(u_QF_InstancePoints[0].xyz, cross(u_QF_InstancePoints[0].xyz, Normal) + Normal*u_QF_InstancePoints[0].w)*2.0 + Normal;
}

void TransformVerts(inout vec4 Position, inout vec3 Normal, inout vec2 TexCoord)
{
}

void TransformVerts(inout vec4 Position, inout vec3 Normal, inout vec3 Tangent, inout vec2 TexCoord)
{
}
vec4 VertexRGBGen(in vec4 Position, in vec3 Normal, in vec4 VertexColor)
{
 vec4 Color = u_ConstColor;
 return Color;
}
void main(void)
{
 vec4 Position = a_Position;
 vec3 Normal = a_Normal.xyz;
 vec2 TexCoord = a_TexCoord;
 vec4 inColor = vec4(a_Color);
 TransformVerts(Position, Normal, TexCoord);
 vec4 outColor = VertexRGBGen(Position, Normal, inColor);
 qf_FrontColor = vec4(outColor);
 v_TexCoord = vec2(dot((u_TextureMatrix)[0],(TexCoord)) + (u_TextureMatrix)[2][0], dot((u_TextureMatrix)[1],(TexCoord)) + (u_TextureMatrix)[2][1]);
 v_Position = Position.xyz;
 v_LightmapTexCoord[0] = a_LightmapCoord0;
 gl_Position = u_ModelViewProjectionMatrix * Position;
}
