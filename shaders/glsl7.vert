#version 420

layout(location = 0) in vec4 v_position;
layout(location = 1) in vec4 v_texcoord;
layout(location = 0) out vec2 source_texture;

void main()
{
    gl_Position = v_position;
    source_texture = v_texcoord.xy;
}
