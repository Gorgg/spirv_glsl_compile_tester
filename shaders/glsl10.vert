#version 420
#extension GL_ARB_texture_rectangle : enable

layout(location = 0) in vec4 vvertex;

layout(std140, binding = 0) uniform vertuniforms {
	mat4 lightmatrix;
};

void main(void) {
	gl_Position = lightmatrix * vvertex;
}
