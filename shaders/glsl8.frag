#version 420

layout(binding = 0) uniform sampler2D _BumpMap;
layout(binding = 1) uniform sampler2D _ReflectionTex;
layout(binding = 2) uniform sampler2D _RefractionTex;
layout(binding = 3) uniform sampler2D _ShoreTex;
layout(binding = 4) uniform sampler2D _CameraDepthTexture;

layout(std140, binding = 1) uniform fraguniforms {
	vec4 _ZBufferParams;
	vec4 _SpecularColor;
	vec4 _BaseColor;
	vec4 _ReflectionColor;
	vec4 _InvFadeParemeter;
	float _Shininess;
	vec4 _WorldLightDir;
	vec4 _DistortParams;
	float _FresnelScale;
	vec4 _Foam;
};

layout(location = 0) in vec4 xlv_TEXCOORD0;
layout(location = 1) in vec4 xlv_TEXCOORD1;
layout(location = 2) in vec4 xlv_TEXCOORD2;
layout(location = 3) in vec4 xlv_TEXCOORD3;
layout(location = 4) in vec4 xlv_TEXCOORD4;
layout(location = 0) out vec4 FragColor;

void main ()
{
  vec4 baseColor_1;
  vec4 edgeBlendFactors_2;
  vec4 rtRefractions_3;
  vec3 worldNormal_4;
  vec3 normal_5;
  normal_5.xy = ((texture(_BumpMap, xlv_TEXCOORD2.xy).wy * 2.0) - 1.0);
  normal_5.z = sqrt((1.0 - clamp (
    dot (normal_5.xy, normal_5.xy)
  , 0.0, 1.0)));
  vec3 normal_6;
  normal_6.xy = ((texture(_BumpMap, xlv_TEXCOORD2.zw).wy * 2.0) - 1.0);
  normal_6.z = sqrt((1.0 - clamp (
    dot (normal_6.xy, normal_6.xy)
  , 0.0, 1.0)));
  vec3 tmpvar_7;
  tmpvar_7 = normalize((xlv_TEXCOORD0.xyz + (
    (((normal_5 + normal_6) * 0.5).xxy * _DistortParams.x)
   * vec3(1.0, 0.0, 1.0))));
  worldNormal_4 = tmpvar_7;
  vec3 tmpvar_8;
  tmpvar_8 = normalize(xlv_TEXCOORD1.xyz);
  vec4 tmpvar_9;
  tmpvar_9.zw = vec2(0.0, 0.0);
  tmpvar_9.xy = ((tmpvar_7.xz * _DistortParams.y) * 10.0);
  vec4 tmpvar_10;
  tmpvar_10 = (xlv_TEXCOORD4 + tmpvar_9);
  vec4 tmpvar_11;
  tmpvar_11 = textureProj (_RefractionTex, xlv_TEXCOORD4);
  rtRefractions_3 = textureProj (_RefractionTex, tmpvar_10);
  vec4 tmpvar_12;
  tmpvar_12 = textureProj (_ReflectionTex, (xlv_TEXCOORD3 + tmpvar_9));
  float tmpvar_13;
  tmpvar_13 = (1.0/(((_ZBufferParams.z * textureProj (_CameraDepthTexture, tmpvar_10).x) + _ZBufferParams.w)));
  if ((tmpvar_13 < xlv_TEXCOORD3.z)) {
    rtRefractions_3 = tmpvar_11;
  };
  vec4 tmpvar_14;
  tmpvar_14 = clamp ((_InvFadeParemeter * (
    (1.0/(((_ZBufferParams.z * textureProj (_CameraDepthTexture, xlv_TEXCOORD3).x) + _ZBufferParams.w)))
   - xlv_TEXCOORD3.w)), 0.0, 1.0);
  edgeBlendFactors_2.xzw = tmpvar_14.xzw;
  edgeBlendFactors_2.y = (1.0 - tmpvar_14.y);
  worldNormal_4.xz = (tmpvar_7.xz * _FresnelScale);
  vec4 tmpvar_15;
  tmpvar_15 = (_BaseColor - ((xlv_TEXCOORD1.w * _InvFadeParemeter.w) * vec4(0.15, 0.03, 0.01, 0.0)));
  vec4 coords_16;
  coords_16 = (xlv_TEXCOORD2 * 2.0);
  baseColor_1.xyz = ((mix (
    mix (rtRefractions_3, tmpvar_15, tmpvar_15.wwww)
  , 
    mix (tmpvar_12, _ReflectionColor, _ReflectionColor.wwww)
  , vec4(
    clamp ((_DistortParams.w + ((1.0 - _DistortParams.w) * pow (
      clamp ((1.0 - max (dot (
        -(tmpvar_8)
      , worldNormal_4), 0.0)), 0.0, 1.0)
    , _DistortParams.z))), 0.0, 1.0)
  )) + (
    max (0.0, pow (max (0.0, dot (tmpvar_7, 
      -(normalize((_WorldLightDir.xyz + tmpvar_8)))
    )), _Shininess))
   * _SpecularColor)).xyz + ((
    ((texture(_ShoreTex, coords_16.xy) * texture(_ShoreTex, coords_16.zw)) - 0.125)
  .xyz * _Foam.x) * (edgeBlendFactors_2.y + 
    clamp ((xlv_TEXCOORD1.w - _Foam.y), 0.0, 1.0)
  )));
  baseColor_1.w = edgeBlendFactors_2.x;
  FragColor = baseColor_1;
}
