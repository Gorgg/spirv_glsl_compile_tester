#version 420

layout(location = 0) in vec4 Position;

void main (void) {
	gl_Position = Position;
}
